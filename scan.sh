#!/bin/sh

version=0.1

format=pdf
resolution=150
mode=Color
x=210
y=297
endParam=0

# Il faut toujours donner le nom de l'output
if [ $# = 0 ]; then
    echo "Veuillez renseigner un nom de fichier"
    exit 1
fi

# On parse les options
until [ $# = 0 ]
do
    if [ $endParam = 1 ]; then
        echo "Les options \"$*\" ne seront pas prise en compte"
        break
    fi

    case "$1" in
        "--format" | "-f" )
            shift
            if [ "$1" = "pdf" ] || [ "$1" = "jpeg" ] || [ "$1" = "png" ] || [ "$1" = "tiff" ]; then
                format=$1
            else
                echo "Le format $1 n'est pas supporté"
                echo "Les formats supporté sont : pdf, jpeg, png et tiff"
                exit 1
            fi;;
        "--resolution" | "-r" )
            shift
            if [ "$1" = "75" ] || [ "$1" = "100" ] || [ "$1" = "150" ] || [ "$1" = "200" ] || [ "$1" = "300" ] || [ "$1" = "600" ]; then
                resolution=$1
            else
                echo "La résolution $1 n'est pas supporté..."
                echo "Les résolutions supporté sont : 75, 100, 150, 200, 300 et 600"
                exit 1
            fi;;
        "--mode" | "-m" )
            shift
            if [ "$1" = "Lineart" ] || [ "$1" = "Halftone" ] || [ "$1" = "Gray" ] || [ "$1" = "Color" ]; then
                mode=$1
            else
                echo "Le mode $1 n'est pas supporté..."
                echo "Les modes disponnibles sont : Lineart, Halftone, Gray, Color"
                exit 1
            fi;;
        "-x" )
            shift
            x=$1;;
        "-y" )
            shift
            y=$1;;
        "--version" | "-v" ) 
            echo "TimScan version v$version"
            exit 0;;
        "--help" | "-h" )
            echo -e "L'utilitaire TimScan permet d'effectuer un scan facilement. Il s'appuie principalement sur les commandes scanimage et convert pour la convertion pdf.\n"
            echo -e "Les paramètre doivent être séparé par des espaces"

            echo -e "\n\t-f, --format pnm|tiff|png|jpeg|pdf [pdf]"
            echo -e "\t\tDéfinir le format de sortie"

            echo -e "\n\t-r, --resolution 75|100|150|200|300|600dpi [150]"
            echo -e "\t\tDéfinir la résolution de l'image scanné"

            echo -e "\n\t-m, --mode Lineart|Halftone|Gray|Color [Color]"
            echo -e "\t\tSelectionner le mode de scan à utiliser"

            echo -e "\n\t-x 0..216.069mm (par pas de 1) [210]"
            echo -e "\t\tLargeur de la zone de scan"

            echo -e "\n\t-y 0..297.18mm (par pas de 1) [297]"
            echo -e "\t\tHauteur de la zone de scan"

            echo -e "\n\t-v, --version"
            echo -e "\t\tAffiche la version actuel de ce script"

            echo -e "\n\t-h, --help"
            echo -e "\t\tAfficher les options disponibles"

            exit 0;;

        * )
            if [ ${1:0:1} = "-" ]; then
                echo "L'option $1 n'est pas reconnus. Tapez la commande avec --help pour avoir de l'aide"
                exit 1
            else
                output=$1
                endParam=1
            fi;;
    esac
    shift # On passe a l'argument suivant
done


# Affichage des options
echo -e "\n"
echo "Format : $format"
echo "Résolution : $resolution"
echo "Mode : $mode"
echo "x : $x"
echo "y : $y"
echo "Fichier de sortie : $output"

echo "progression..."

convert=0
if [ "$format" = "pdf" ]; then
    format=tiff
    convert=1;
    scanimage --format $format --resolution $resolution --mode $mode -x $x -y $y > $output.tiff
else
    scanimage --format $format --resolution $resolution --mode $mode -x $x -y $y > $output
fi


if [ $convert = 1 ]; then
    convert $output.tiff $output
    rm $output.tiff
fi

echo -n "Ouvrir le fichier ? [o/N]"
read openFile

if [ "$openFile" = "o" ] || [ "$openFile" = "O" ]; then
    case $format in
        "tiff" )
            if [ $convert = 1 ]; then
                # C'était un format pdf
                epdfview $output > /dev/null 2>&1
            else
                # C'est vraiment un tiff
                viewnior $output > /dev/null 2>&1
            fi;;
        "png" | "jpeg" )
            viewnior $output > /dev/null 2>&1;;
        * )
            echo "Nous ne pouvons malheureusement pas encore ouvrir les fichiers de type $format";;
    esac
fi
